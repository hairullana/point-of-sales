<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Product;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name' => 'Baju Anak',
            'price' => 100000
        ]);
        Product::create([
            'name' => 'Jas',
            'price' => 250000
        ]);
        Product::create([
            'name' => 'Kaos Kaki',
            'price' => 30000
        ]);
        Product::create([
            'name' => 'Celana Panjang',
            'price' => 150000
        ]);
        Product::create([
            'name' => 'Jaket',
            'price' => 200000
        ]);
    }
}
