<?php

namespace App\Http\Controllers;

use App\Models\Stok;
use App\Models\Barang;
use App\Models\Caseer;
use App\Models\History;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class PosController extends Controller
{
    public function index()
    {
        $products = Product::all();

        return view('pos', [
            'products' => $products,
            'products' => $products
        ]);
    }

    public function pay(Request $request)
    {
        $totalHistory = Caseer::groupBy('history')->get()->count();

        foreach($request->carts as $id => $quantity) {
            Caseer::create([
                'product_id' => $id,
                'quantity' => $quantity,
                'history' => $totalHistory +1
            ]);
        }

        return redirect()->back();
    }

    public function getProductDetail(Request $request)
    {
        $product = Product::find($request->id);

        return response()->json([
            'product' => $product
        ]);
    }
}
